// THis is based on the example shown here:
// https://www.codeshiftcommunity.com/docs/your-first-codemod

export default function transform(fileInfo, api, options) {
  const j = api.jscodeshift;
  const root = j(fileInfo.source);

  const imports = root
    .find(j.ImportDeclaration)
    .filter(path => path.node.source.value === 'my-module');

  console.log('Filtered out import node is', imports);

  return root.toSource(options.printOptions); // We return the modified file
}
