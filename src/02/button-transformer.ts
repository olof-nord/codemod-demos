import { API, FileInfo, Options } from "jscodeshift";

// This example is based on this example
// https://github.com/chimurai/jscodeshift-typescript-example/blob/main/examples/simple-rename.ts

export default function transform(file: FileInfo, api: API, _options: Options) {
  const j = api.jscodeshift;
  const root = j(file.source);

  const variableDeclarators = root.findVariableDeclarators("printHello");
  variableDeclarators.renameTo("bar");

  return root.toSource();
}
