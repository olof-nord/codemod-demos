import { defineTest } from 'jscodeshift/src/testUtils';
import { Options } from "jscodeshift";

describe("Button Transformer", () => {

  // this is important to specify, as the default is babel
  const transformOptions: Options = { parser: "tsx" };

  // the second argument (transformName) is interpreted as 'src/02/button-transformer.ts'
  // The fourth argument (testFilePrefix) is interpreted as 'src/02/__testfixtures__/HelloButton.input.tsx'

  defineTest(__dirname, 'button-transformer', null, 'HelloButton', transformOptions);
  defineTest(__dirname, 'button-transformer', null, 'GoodByeButton', transformOptions);
});
