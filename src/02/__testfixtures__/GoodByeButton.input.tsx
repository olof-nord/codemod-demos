import React from "react";

const GoodByeButton: React.FC = () => {
  const printHello = () => {
    alert('Goodbye!');
  };

  return (
    <>
      <button onClick={printHello}>Click me to say goodbye</button>
    </>
  );
};

export default GoodByeButton;
