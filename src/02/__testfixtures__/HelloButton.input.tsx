import React from "react";

const HelloButton: React.FC = () => {
  const printHello = () => {
    alert('Hello!');
  };

  return (
    <>
      <button onClick={printHello}>Click me to say hello</button>
    </>
  );
};

export default HelloButton;
