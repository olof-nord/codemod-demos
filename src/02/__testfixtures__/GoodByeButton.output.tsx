import React from "react";

const GoodByeButton: React.FC = () => {
  const bar = () => {
    alert('Goodbye!');
  };

  return (<>
    <button onClick={bar}>Click me to say goodbye</button>
  </>);
};

export default GoodByeButton;
