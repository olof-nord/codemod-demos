import React from "react";

const HelloButton: React.FC = () => {
  const bar = () => {
    alert('Hello!');
  };

  return (<>
    <button onClick={bar}>Click me to say hello</button>
  </>);
};

export default HelloButton;
