# codemod-demos

This repo contains some small codemods, code transformation scripts built with (and for) `jscodeshift`.

jscodeshift provides a way to work with AST data structures: finding and editing code as objects and arrays, rather than
strings.

## Examples

1. [My First Codemod](./src/01/my-first-codemod.js)
2. [A TypeScript TSX button transformer](./src/02/button-transformer.ts)

## Resources

* [astexplorer.net](https://astexplorer.net/)
* [codeshiftcommunity.com](https://www.codeshiftcommunity.com/)
* [a collection of codemod scripts](https://github.com/cpojer/js-codemod)
